package org.monEntreprise;

public class TestCode {
    public static void main(String[] args) {
        // Définir les noms des allées dans l'avion
        char[] nomsAllees = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};



        // Créer la structure pour les rangées et les allées
        String[][] siegeTotal = new String[26][9];



        // Remplir la structure avec les noms des allées
        for (int rangée = 0; rangée < 26; rangée++) {
//            PRINT DE RANGEE
            for (int allée = 0; allée < 9; allée++) {
                // Crée une chaîne de caractères avec le numéro de rangée et le nom de l'allée
                siegeTotal[rangée][allée] = "siege " + (rangée + 1) + nomsAllees[allée];
            }
        }



        // Afficher la structure de l'avion
        for (int rangée = 0; rangée < 26; rangée++) {
            for (int allée = 0; allée < 9; allée++) {
                // Affiche le siège de la rangée et de l'allée actuels
                System.out.print(siegeTotal[rangée][allée] + "\t");
            }
            // Passe à la ligne suivante après avoir affiché une rangée complète d'allées
            System.out.println();
        }
    }
}