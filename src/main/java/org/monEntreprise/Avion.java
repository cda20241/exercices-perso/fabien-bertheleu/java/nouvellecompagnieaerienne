package org.monEntreprise;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Avion {
    private String modele;
    private List<Siege> sieges;

    public Avion(String modele) {
        this.modele = modele;
        Siege siege1 = new Siege("A", 1);
        Siege siege2 = new Siege("B", 1);
        this.sieges = new ArrayList<>();
        this.sieges.add(siege1);
        this.sieges.add(siege2);
    }

    public String getModele() {
        return modele;
    }

    public List<Siege> getSieges() {
        return sieges;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Entrez le nombre maximal de rangées : ");
        int rangeMax = scanner.nextInt();


//        System.out.println("Entrez le nombre maximal d'allées : ");
//        int alleeMax = scanner.nextInt();
        System.out.print("Entrez une lettre de A à I: ");
        char alleeMax = scanner.next().charAt(0);

        // Définir les noms des allées dans l'avion
        char[] nomsAllees = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'};

        // Créer la matrice de sièges
        int[][][] matriceSieges = new int[rangeMax][alleeMax][1];
//        System.out.println(rangeMax +""+ alleeMax);
        // Initialiser tous les sièges comme libres (0 signifie libre, 1 signifie occupé)
        for (int rangée = 0; rangée < rangeMax; rangée++) {
            for (int allée = 0; allée < alleeMax; allée++) {
                if (
                        matriceSieges[rangée][allée][0] == 0) {
                    // 0 signifie libre
                    System.out.println(rangée + "" + nomsAllees[allée] + " est libre");
                } else {
                    System.out.println(rangée + "" + nomsAllees[allée] + " est déja réservé");
                }

            }

        }
    }
}
