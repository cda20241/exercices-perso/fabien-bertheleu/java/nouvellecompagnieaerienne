package org.monEntreprise;

import java.util.*;
public class StructureAvionBis {
    private static Scanner scanner = new Scanner(System.in);
    /**
     * Méthode principale du programme. Elle permet à l'utilisateur d'interagir avec le service de réservation de sièges d'avion.
     *
     * @param args Les arguments de la ligne de commande (non utilisés dans ce programme).
     */
    public static void main(String[] args) {
        // Crée une instance de Scanner pour lire l'entrée utilisateur.
        System.out.print("Entrez la lettre maximale de l'allée (de A à I) : ");
        // Lit la lettre maximale de l'allée (exemple : A, B, C, etc.) et la convertit en majuscules.
        char alleeMax = scanner.next().charAt(0);
        alleeMax = Character.toUpperCase(alleeMax);
        // Demande à l'utilisateur d'entrer le nombre maximal de rangées et le lit.
        System.out.print("Entrez le nombre maximal de rangées : ");
        int rangMax = scanner.nextInt();
        // Passe à la ligne suivante pour lire les entrées suivantes.
        scanner.nextLine();
        // Initialise un dictionnaire de sièges avec les informations de l'allée maximale et du nombre de rangées.
        Map<String, Boolean> sieges = initialiserSieges(alleeMax, rangMax);
        // Variable pour gérer la boucle principale du menu.
        boolean continuer = true;
        // Boucle principale du menu.
        while (continuer) {
            // Affiche le menu.
            System.out.println("Menu :");
            System.out.println("1. Voir les places restantes");
            System.out.println("2. Réserver une place");
            System.out.println("3. Afficher les places occupées et les places restantes");
            System.out.println("4. Voir les places côté hublot");
            System.out.println("5. Quitter");
            System.out.print("Choisissez une option (1/2/3/4/5) : ");
            // Lit le choix de l'utilisateur.
            int choix = scanner.nextInt();
            // Passe à la ligne suivante pour lire les entrées suivantes.
            scanner.nextLine();
            // Utilise une structure de commutation pour gérer les différentes options du menu.
            switch (choix) {
                case 1:
                    // Option : Voir les places restantes.
                    afficherPlacesRestantes(sieges, alleeMax, rangMax);
                    break;
                case 2:
                    // Option : Réserver une place.
                    reserverUnePlace(sieges, alleeMax, rangMax);
                    break;
                case 3:
                    // Option : Afficher les places occupées et les places restantes.
                    afficherPlacesOccupees(sieges, alleeMax, rangMax);
                    break;
                case 4:
                    // Option : Voir les places côté hublot.
                    afficherSiegesCoteHublot(sieges, alleeMax, rangMax);
                    break;
                case 5:
                    // Option : Quitter.
                    continuer = false;
                    break;
                default:
                    // Option invalide.
                    System.out.println("Option invalide. Veuillez choisir une option valide.");
            }
        }
        // Sortie lorsque l'utilisateur choisit de quitter.
        System.out.println("Merci d'avoir utilisé notre service de réservation de sièges !");
    }

    /**
     * Initialise un dictionnaire de sièges avec leur état initial de disponibilité.
     *
     * @param alleeMax La lettre maximale de l'allée.
     * @param rangMax  Le nombre maximal de rangées.
     * @return Un dictionnaire de sièges initialisés avec des clés de type String et des valeurs de type Boolean.
     */
    private static Map<String, Boolean> initialiserSieges(char alleeMax, int rangMax) {
        // Crée une nouvelle instance de Map (dictionnaire) avec des clés de type String et des valeurs de type Boolean.
        Map<String, Boolean> sieges = new HashMap<>();
        // Parcours de toutes les allées de 'A' à la lettre maximale de l'allée (alleeMax).
        for (char allee = 'A'; allee <= alleeMax; allee++) {
            // Parcours de tous les rangs, de 1 jusqu'au nombre maximal de rangées (rangMax).
            for (int rang = 1; rang <= rangMax; rang++) {
                // Crée une chaîne de caractères qui représente l'emplacement du siège en combinant l'allée et le rang.
                String place = String.valueOf(allee) + rang;
                // Associe cet emplacement (place) à la valeur "false" dans le dictionnaire (siège non réservé).
                sieges.put(place, false);
            }
        }
        // Renvoie le dictionnaire (Map) contenant tous les sièges initialisés.
        return sieges;
    }

    /**
     * Affiche les places restantes dans l'avion pour chaque allée.
     *
     * @param sieges   Dictionnaire de sièges avec leur état de disponibilité (clé : emplacement, valeur : état).
     * @param alleeMax La lettre maximale de l'allée.
     * @param rangMax  Le nombre maximal de rangées.
     */
    private static void afficherPlacesRestantes(Map<String, Boolean> sieges, char alleeMax, int rangMax) {
        for (char allee = 'A'; allee <= alleeMax; allee++) {
            // Affiche le libellé de l'allée.
            System.out.print("Siège Allée " + allee + " Restant : ");
            for (int rang = 1; rang <= rangMax; rang++) {
                // Crée une chaîne de caractères représentant l'emplacement du siège en combinant l'allée et le rang.
                String place = String.valueOf(allee) + rang;
                // Vérifie si le siège correspondant dans le dictionnaire (sieges) est disponible (valeur : false).
                if (!sieges.get(place)) {
                    // Affiche l'emplacement du siège qui est encore disponible.
                    System.out.print(place + " ");
                }
            }
            // Passe à la ligne suivante pour la prochaine allée.
            System.out.println();
        }
        // Affiche un message pour demander à l'utilisateur d'appuyer sur Entrée pour continuer.
        System.out.println("Appuyez sur Entrée pour continuer...");
        // Attend que l'utilisateur appuie sur Entrée en utilisant le scanner.
        scanner.nextLine();
    }

    /**
     * Permet à l'utilisateur de réserver une place dans l'avion.
     *
     * @param sieges   Dictionnaire de sièges avec leur état de disponibilité (clé : emplacement, valeur : état).
     * @param alleeMax La lettre maximale de l'allée.
     * @param rangMax  Le nombre maximal de rangées.
     */
    private static void reserverUnePlace(Map<String, Boolean> sieges, char alleeMax, int rangMax) {
        // Invite l'utilisateur à entrer l'allée et le rang de la place à réserver (par exemple, "I12").
        System.out.print("Entrez l'allée et le rang de la place à réserver (exemple, I12) : ");
        // Lit l'entrée de l'utilisateur et la convertit en majuscules.
        String alleeRangSaisie = scanner.nextLine().toUpperCase();
        // Vérifie si l'entrée correspond au format attendu (lettre d'allée suivie d'un ou plusieurs chiffres).
        if (alleeRangSaisie.matches("[A-" + alleeMax + "][1-9][0-9]*")) {
            // Extrait la lettre de l'allée et le rang à partir de l'entrée.
            char alleeSaisie = alleeRangSaisie.charAt(0);
            int rangSaisi = Integer.parseInt(alleeRangSaisie.substring(1));
            // Vérifie si l'allée saisie est valide et si le rang saisi est dans la plage autorisée.
            if (alleeSaisie >= 'A' && alleeSaisie <= alleeMax && rangSaisi >= 1 && rangSaisi <= rangMax) {
                // Crée une chaîne de caractères représentant l'emplacement de la place à réserver.
                String placeSaisie = String.valueOf(alleeSaisie) + rangSaisi;
                // Vérifie si la place correspondante dans le dictionnaire (sieges) est disponible (valeur : false).
                if (!sieges.get(placeSaisie)) {
                    // Affiche un message indiquant que la place est libre.
                    System.out.println("La place " + placeSaisie + " est libre.");
                    // Demande à l'utilisateur de confirmer la réservation (Oui/Non).
                    System.out.print("Voulez-vous la réserver ? (Oui/Non) : ");
                    // Lit la réponse de l'utilisateur et la supprime des espaces inutiles.
                    String validerReservation = scanner.nextLine().trim();
                    // Vérifie si l'utilisateur a confirmé la réservation (réponse "Oui" ou "O").
                    if (validerReservation.equalsIgnoreCase("Oui") || validerReservation.equalsIgnoreCase("O")) {
                        // Vérifie si la place à réserver est côté hublot.
                        if (estSiegeCoteHublot(alleeMax, rangSaisi, rangMax)) {
                            // Affiche un message indiquant que la place est maintenant réservée côté hublot.
                            System.out.println("Le siège " + placeSaisie + " côté hublot est maintenant réservé.");
                            // Attend que l'utilisateur appuie sur Entrée pour continuer.
                            System.out.println("Appuyez sur Entrée pour continuer...");
                            scanner.nextLine();
                        } else {
                            // Affiche un message indiquant que la place est maintenant réservée.
                            System.out.println("Le siège " + placeSaisie + " est maintenant réservé.");
                            // Attend que l'utilisateur appuie sur Entrée pour continuer.
                            System.out.println("Appuyez sur Entrée pour continuer...");
                            scanner.nextLine();
                        }
                        // Met à jour l'état de la place dans le dictionnaire (siège réservé : valeur = true).
                        sieges.put(placeSaisie, true);
                    } else {
                        // Affiche un message indiquant que la place n'a pas été réservée.
                        System.out.println("La place " + placeSaisie + " n'a pas été réservée.");
                        System.out.println("Appuyez sur Entrée pour continuer...");
                        // Attend que l'utilisateur appuie sur Entrée pour continuer.
                        scanner.nextLine();
                    }
                } else {
                    // Affiche un message indiquant que la place est déjà occupée.
                    System.out.println("Désolé, la place " + placeSaisie + " est déjà occupée. Veuillez choisir une autre place.");
                }
            } else {
                // Affiche un message d'erreur pour une place non valide.
                System.out.println("Place non valide. Veuillez choisir une place existante.");
            }
        } else {
            // Affiche un message d'erreur pour une saisie incorrecte.
            System.out.println("Saisie incorrecte. Veuillez entrer une place au format correct (par exemple, I12).");
        }
    }

    /**
     * Affiche le nombre de places occupées et le nombre de places restantes dans l'avion.
     *
     * @param sieges   Dictionnaire de sièges avec leur état de disponibilité (clé : emplacement, valeur : état).
     * @param alleeMax La lettre maximale de l'allée.
     * @param rangMax  Le nombre maximal de rangées.
     */
    private static void afficherPlacesOccupees(Map<String, Boolean> sieges, char alleeMax, int rangMax) {
        // Initialise le compteur du nombre de places occupées.
        int placesOccupees = 0;
        // Parcours de toutes les allées de 'A' à la lettre maximale de l'allée (alleeMax).
        for (char allee = 'A'; allee <= alleeMax; allee++) {
            // Parcours de tous les rangs, de 1 jusqu'au nombre maximal de rangées (rangMax).
            for (int rang = 1; rang <= rangMax; rang++) {
                // Crée une chaîne de caractères qui représente l'emplacement du siège en combinant l'allée et le rang.
                String place = String.valueOf(allee) + rang;
                // Vérifie si le siège correspondant dans le dictionnaire (sieges) est occupé (valeur : true).
                if (sieges.get(place)) {
                    // Incrémente le compteur du nombre de places occupées.
                    placesOccupees++;
                }
            }
        }
        // Calcule le nombre total de places dans l'avion.
        int totalPlaces = (alleeMax - 'A' + 1) * rangMax;
        // Calcule le nombre de places restantes en soustrayant les places occupées du total.
        int placesRestantes = totalPlaces - placesOccupees;
        // Affiche le nombre de places occupées et le nombre de places restantes.
        System.out.println("Places occupées : " + placesOccupees + " / " + totalPlaces);
        System.out.println("Places restantes : " + placesRestantes + " / " + totalPlaces);
        // Invite l'utilisateur à appuyer sur Entrée pour continuer.
        System.out.println("Appuyez sur Entrée pour continuer...");
        scanner.nextLine();
    }

    /**
     * Affiche la liste des sièges côté hublot restants.
     *
     * @param sieges   Dictionnaire de sièges avec leur état de disponibilité (clé : emplacement, valeur : état).
     * @param alleeMax La lettre maximale de l'allée.
     * @param rangMax  Le nombre maximal de rangées.
     */
    private static void afficherSiegesCoteHublot(Map<String, Boolean> sieges, char alleeMax, int rangMax) {
        // Affiche un titre pour la liste des sièges côté hublot.
        System.out.println("Liste des sièges côté hublot restants :");
        // Parcours de tous les rangs, de 1 jusqu'au nombre maximal de rangées (rangMax).
        for (int rang = 1; rang <= rangMax; rang++) {
            // Définit la lettre de l'allée du côté gauche de l'avion comme 'A'.
            char alleeGauche = 'A';
            // Définit la lettre de l'allée du côté droit de l'avion comme la lettre maximale de l'allée (alleeMax).
            char alleeDroite = alleeMax;
            // Crée les chaînes de caractères représentant les emplacements des sièges côté gauche et côté droit du hublot.
            String placeGauche = String.valueOf(alleeGauche) + rang;
            String placeDroite = String.valueOf(alleeDroite) + rang;
            // Vérifie si le siège côté gauche est disponible (non réservé).
            if (!sieges.get(placeGauche)) {
                // Affiche l'emplacement du siège côté gauche.
                System.out.print(placeGauche + " ");
            }
            // Vérifie si le siège côté droit est disponible (non réservé).
            if (!sieges.get(placeDroite)) {
                // Affiche l'emplacement du siège côté droit.
                System.out.print(placeDroite + " ");
            }
        }
        // Affiche une ligne vide pour séparer la liste des sièges côté hublot de l'invite suivante.
        System.out.println();
        // Invite l'utilisateur à appuyer sur Entrée pour continuer.
        System.out.println("Appuyez sur Entrée pour continuer...");
        scanner.nextLine();
    }

    /**
     * Vérifie si un siège est côté hublot.
     *
     * @param alleeMax La lettre maximale de l'allée.
     * @param rang     Le numéro du rang du siège.
     * @param rangMax  Le nombre maximal de rangées.
     * @return true si le siège est côté hublot, sinon false.
     */
    private static boolean estSiegeCoteHublot(char alleeMax, int rang, int rangMax) {
        // Un siège est côté hublot si l'une des conditions suivantes est vraie :
        // 1. Il est situé au premier rang (rang == 1).
        // 2. Il est situé au dernier rang (rang == rangMax).
        // 3. Il est situé du côté de l'allée A (alleeMax == 'A').
        // 4. Il est situé du côté de l'allée I (alleeMax == 'I').
        return rang == 1 || rang == rangMax || alleeMax == 'A' || alleeMax == 'I';
    }
}