package org.monEntreprise;

import java.util.Date;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Entrée with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        Compagnie compagnie1 = new Compagnie("A");
        Compagnie compagnie2 = new Compagnie("B");
        compagnie1.creerVol("Berlin", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnie1.creerVol("Paris", "Tokyo",
                new Date(2023, 10, 17),
                new Date(2023, 10, 18));
        compagnie1.afficherVols();
        List<Vol> volsParis = compagnie1.getVolsByVilleDepart("Paris");
        for (Vol vol : volsParis) {
            compagnie1.annulerVol(vol);
        }
        System.out.println("Vols pour Paris annulés");
        compagnie1.afficherVols();
    }
}